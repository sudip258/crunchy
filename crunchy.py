#!/usr/bin/env python

import ast
import sys
from random import choice

from pymongo import MongoClient
import requests
import yaml

BASE_URL = 'http://api.crunchbase.com/v/1/'
ACCESS_TOKENS = ['6ce5drjarahtq7sx9gm7tj6h',
    '855vggsggk8bqkq2njf4d4x8',
    'xt98fespd6fsgz6yehukpjzz',
    'm9ffkwex3ktzua2hzh9nqkch',
    ]

# Assuming mongodb server is running at http://127.0.0.1:27017
client = MongoClient()
db = client.database
posts = db.posts

def fetch_lists(whom):
    print "Fetching {some} list.".format(some=whom)
    response = requests.get(BASE_URL+whom+'.js?api_key='+choice(ACCESS_TOKENS), stream=True)
    body = []
    print "Connection established. Collecting data..."
    for chunk in response.iter_content(1024):
        body.append(chunk)
#        print chunk
    print "Data collection completed."
    return body

def companies():
    print "Fetching companies list, this will take little time."
    body = fetch_lists('companies')
    with open('companies.tmp', 'w') as fp:
        fp.write("".join(body).replace('null}', '"Null"}')[2:-3])
    print "Inserting data for companies list from crunchbase:"
    with open('companies.tmp', 'r') as fp:
        for aline in fp:
            line = ast.literal_eval("{"+aline.split(",")[1]+"}")
            if line.get("permalink", "404"):
                response = requests.get(BASE_URL+"company/"+line.get("permalink", "404")+".js?api_key="+choice(ACCESS_TOKENS))
                if response.status_code in [200, 301]:
                    data = yaml.load(yaml.load(yaml.dump(response.text.encode('ascii', 'ignore'), allow_unicode=True)))
#                   data = yaml.load(response.text.encode('ascii', 'ignore').replace('#', ""))
                    posts.insert(data)
                    print "Inserted data for Company: {0}".format(data.get('name').encode('ascii', 'ignore'))
#                    break
    print "Data insertion for companies have finished successfully.\n"

def people():
    body = fetch_lists('people')
    with open('people.tmp', 'w') as fp:
        fp.write("".join(body).replace('][', ',')[2:-3])
    print "Inserting data for people from crunchbase:"
    with open('people.tmp', 'r') as fp:
        for aline in fp:
#            line = aline[2:].replace(' ', '').replace(',\n', '').replace('}', '')
            line = ast.literal_eval(aline.split(",")[0]+"}")
            if line.get("permalink", "404"):
                response = requests.get(BASE_URL+"person/"+line.get("permalink", "404")+".js?api_key="+choice(ACCESS_TOKENS), stream=True)
                if response.status_code in [200, 301]:
                    body = []
                    for chunk in response.iter_content(1024):
                        body.append(chunk)
                    data = yaml.load(yaml.load(yaml.dump("".join(body).encode('ascii', 'ignore'), allow_unicode=True)))
#                    data = yaml.load("".join(body).encode('ascii', 'ignore').replace('#', ""))
                    print "Inserted data for: {first} {last}".format(first=data.get('first_name'), last=data.get('last_name')).encode('ascii', 'ignore')
                    posts.insert(data)
#                    break
    print "Data insertion for people have finished successfully.\n"

    
def financial_organizations():
    body = fetch_lists('financial-organizations')
    with open('financial_organizations.tmp', 'w') as fp:
        fp.write("".join(body).replace('null}', '"Null"}')[2:-3])
    print "Inserting data for financial organizations from crunchbase:"
    with open('financial_organizations.tmp', 'r') as fp:
        for aline in fp:
#            line = aline[2:].replace(' ', '').replace(',\n', '').replace('}', '')
            line = ast.literal_eval(aline.split(",")[0]+"}")
            if line.get("permalink", "404"):
                response = requests.get(BASE_URL+"financial-organization/"+line.get("permalink", "404")+".js?api_key="+choice(ACCESS_TOKENS), stream=True)
                if response.status_code in [200, 301]: 
                    body = []
                    for chunk in response.iter_content(1024):
                        body.append(chunk)
                    data = yaml.load(yaml.load(yaml.dump("".join(body).encode('ascii', 'ignore'), allow_unicode=True)))
#                   data = yaml.load("".join(body).encode('ascii', 'ignore').replace('#', ""))
                    print "Inserted data for organization: {0}".format(data.get('name').encode('ascii', 'ignore'))
                    posts.insert(data)
#                    break
    print "Data insertion for financial organizations have finished successfully.\n"


def service_providers():
    body = fetch_lists('service-providers')
    with open('service_providers.tmp', 'w') as fp:
        fp.write("".join(body).replace('null}', '"Null"}')[2:-3])
    print "Inserting data for service-providers from crunchbase:"
    with open('service_providers.tmp', 'r') as fp:
        for aline in fp:
#            line = aline[2:].replace(' ', '').replace(',\n', '').replace('}', '')
            line = ast.literal_eval(aline.split(",")[0]+"}")
            if line.get("permalink", "404"):
                response = requests.get(BASE_URL+"service-provider/"+line.get("permalink", "404")+".js?api_key="+choice(ACCESS_TOKENS), stream=True)
                if response.status_code in [200, 301]:
                    body = []
                    for chunk in response.iter_content(1024):
                        body.append(chunk)
                    data = yaml.load(yaml.load(yaml.dump("".join(body).encode('ascii', 'ignore'), allow_unicode=True)))
#                   data = yaml.load("".join(body).encode('ascii', 'ignore').replace('#', ""))
                    print "Inserted data for service provider: {0}".format(data.get('name').encode('ascii', 'ignore'))
                    posts.insert(data)
#                    break
    print "Data insertion for service-providers have finished successfully.\n"


def main():
    try:
        companies()
        people()
        financial_organizations()
        service_providers()
    except KeyboardInterrupt:
        sys.exit(-1)


if __name__ == '__main__':
    main()