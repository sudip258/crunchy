CRunchy
=========

Project requirement
-------------------

Gather data from http://www.crunchbase.com/


Requirements
------------

PyYAML::

    $ pip install PyYAMl

requests::

    $ pip install requests

    
How to run
----------

To run the code ::

    $ python crunchy,py

Press [ctrl]+c to stop the process.

